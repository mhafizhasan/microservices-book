# Microservices Board

### References  

* [microservices](https://microservices.io/)  
* [12factor](https://12factor.net/)  

### Orchestration  

* [temporal](https://temporal.io/)

### Testing  

* [pact](https://pact.io/)  

### Metric  

* [microprofile](https://microprofile.io/)  

### Web Assembly  

* [wasm](https://webassembly.org/)
